package com.skyeye.coupon.dao;

import com.skyeye.coupon.entity.CouponStore;
import com.skyeye.eve.dao.SkyeyeBaseMapper;

public interface CouponStoreDao extends SkyeyeBaseMapper<CouponStore> {
}
