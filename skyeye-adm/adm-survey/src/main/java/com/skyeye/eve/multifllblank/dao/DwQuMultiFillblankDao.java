package com.skyeye.eve.multifllblank.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.multifllblank.entity.DwQuMultiFillblank;

public interface DwQuMultiFillblankDao extends SkyeyeBaseMapper<DwQuMultiFillblank> {
}
