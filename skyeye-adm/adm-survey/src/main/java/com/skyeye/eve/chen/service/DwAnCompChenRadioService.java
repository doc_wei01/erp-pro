/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.eve.chen.service;


import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.common.object.InputObject;
import com.skyeye.common.object.OutputObject;
import com.skyeye.eve.chen.entity.DwAnCompChenRadio;

import java.util.List;

/**
 * @ClassName: DwAnCompChenRadioService
 * @Description: 答卷复合矩阵单选题接口层
 * @author: skyeye云系列--卫志强
 * @date: 2021/5/16 23:21
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye-report Inc. All rights reserved.
 * 注意：本内容具体规则请参照readme执行，地址：https://gitee.com/doc_wei01/skyeye-report/blob/master/README.md
 */
public interface DwAnCompChenRadioService extends SkyeyeBusinessService<DwAnCompChenRadio> {
    void queryDwAnCompChenRadioListById(InputObject inputObject, OutputObject outputObject);

    List<DwAnCompChenRadio> selectBySurveyId(String surveyId);

    List<DwAnCompChenRadio> selectByQuId(String id);
}
