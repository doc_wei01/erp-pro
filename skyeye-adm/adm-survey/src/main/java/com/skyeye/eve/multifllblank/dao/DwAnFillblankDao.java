package com.skyeye.eve.multifllblank.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.multifllblank.entity.DwAnFillblank;

public interface DwAnFillblankDao extends SkyeyeBaseMapper<DwAnFillblank> {
}
