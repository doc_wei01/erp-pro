package com.skyeye.eve.order.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.order.entity.DwAnOrder;

/**
 * @ClassName: ExamAnOrderDao
 * @Description: 答卷 评分题数据层
 * @author: skyeye云系列--lyj
 * @date: 2024/7/16 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */

public interface DwAnOrderDao extends SkyeyeBaseMapper<DwAnOrder> {
}
