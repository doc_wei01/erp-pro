package com.skyeye.eve.multifllblank.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.multifllblank.entity.DwAnDfillblank;

public interface DwAnDfillblankDao extends SkyeyeBaseMapper<DwAnDfillblank> {
}
