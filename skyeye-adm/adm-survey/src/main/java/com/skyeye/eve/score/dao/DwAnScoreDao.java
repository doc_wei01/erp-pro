package com.skyeye.eve.score.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.score.entity.DwAnScore;

public interface DwAnScoreDao extends SkyeyeBaseMapper<DwAnScore> {
}
