package com.skyeye.eve.forum.controller;

import com.skyeye.annotation.api.Api;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: ForumStatisticsDayController
 * @Description: 论坛贴子每日的统计表管理控制层
 * @author: skyeye云系列--卫志强
 * @date: 2021/7/24 11:48
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */

@RestController
@Api(value = "论坛贴子每日的统计表管理", tags = "论坛贴子每日的统计表管理", modelName = "论坛贴子每日的统计表管理")
public class ForumStatisticsDayController {

}
