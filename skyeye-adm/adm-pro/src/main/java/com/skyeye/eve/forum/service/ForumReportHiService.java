package com.skyeye.eve.forum.service;

import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.eve.forum.entity.ForumReportHi;


/**
 * @ClassName: ForumReportHiService
 * @Description: 论坛举报历史表
 * @author: skyeye云系列--卫志强
 * @date: 2022/8/9 9:22
 * @Copyright: 2022 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface ForumReportHiService extends SkyeyeBusinessService<ForumReportHi> {
}
