package com.skyeye.eve.forum.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.forum.entity.ForumNotice;

public interface ForumNoticeDao extends SkyeyeBaseMapper<ForumNotice> {
}
