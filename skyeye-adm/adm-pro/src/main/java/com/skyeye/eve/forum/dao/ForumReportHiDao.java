package com.skyeye.eve.forum.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.eve.forum.entity.ForumReportHi;

/**
 * @ClassName: ForumReportHiDao
 * @Description: 论坛举报历史表dao
 * @author: skyeye云系列--卫志强
 * @date: 2021/7/24 9:09
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface ForumReportHiDao extends SkyeyeBaseMapper<ForumReportHi> {
}
