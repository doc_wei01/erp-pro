/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.checkwork.service;

import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.checkwork.entity.SysEveUserStaffTime;

/**
 * @ClassName: SysEveUserStaffTimeService
 * @Description: 员工考勤时间管理服务类
 * @author: skyeye云系列--卫志强
 * @date: 2025/3/12 22:15
 * @Copyright: 2025 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface SysEveUserStaffTimeService extends SkyeyeBusinessService<SysEveUserStaffTime> {

    boolean checkIsExistByTimeId(String timeId);

}
