/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.common.constans;

/**
 * @ClassName: Constants
 * @Description: 系统公共常量类
 * @author: skyeye云系列--卫志强
 * @date: 2021/6/6 23:22
 * @Copyright: 2021 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public class Constants {

    /**
     * 保存模板说明的redis的key
     */
    public static final String REDIS_CODEMODEL_EXPLAIN_EXEXPLAIN = "exexplaintocodemodel";

    public static String getSysExplainExexplainRedisKey(Integer type) {
        return REDIS_CODEMODEL_EXPLAIN_EXEXPLAIN + "_" + type;
    }

    // win系统桌面图片列表的redis的key
    public static final String SYS_WIN_BG_PIC_REDIS_KEY = "sys_win_bg_pic_redis_key";

    // win系统锁屏桌面图片列表的redis的key
    public static final String SYS_WIN_LOCK_BG_PIC_REDIS_KEY = "sys_win_lock_bg_pic_redis_key";

    // win系统主题颜色列表的redis的key
    public static final String SYS_WIN_THEME_COLOR_REDIS_KEY = "sys_win_theme_color_redis_key";

    // 聊天获取当前登陆用户信息在redis中的key
    public static final String SYS_TALK_USER_THIS_MATN_MATION = "sys_talk_user_this_matn_mation_";

    public static String getSysTalkUserThisMainMationById(String id) {
        return SYS_TALK_USER_THIS_MATN_MATION + id;
    }

    // 聊天获取当前登陆用户拥有的群组列表在redis中的key
    public static final String SYS_TALK_USER_HAS_GROUP_LIST_MATION = "sys_talk_user_has_group_list_mation_";

    public static String getSysTalkUserHasGroupListMationById(String id) {
        return SYS_TALK_USER_HAS_GROUP_LIST_MATION + id;
    }

    // 聊天获取分组下的用户列表信息在redis中的key
    public static final String SYS_TALK_GROUP_USER_LIST_MATION = "sys_talk_group_user_list_mation_";

    public static String getSysTalkGroupUserListMationById(String id) {
        return SYS_TALK_GROUP_USER_LIST_MATION + id;
    }

    // 获取群组成员列表
    public static final String SYS_EVE_TALK_GROUP_USER_LIST = "sys_eve_talk_group_user_list_";

    public static String checkSysEveTalkGroupUserListByGroupId(String groupId) {
        return SYS_EVE_TALK_GROUP_USER_LIST + groupId;
    }

}
