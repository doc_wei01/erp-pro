package com.skyeye.office.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.office.entity.DocumentOnlineUser;


public interface DocumentOnlineUserDao extends SkyeyeBaseMapper<DocumentOnlineUser> {
}
