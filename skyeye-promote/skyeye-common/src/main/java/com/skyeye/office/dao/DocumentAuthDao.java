package com.skyeye.office.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.office.entity.DocumentAuth;

public interface DocumentAuthDao extends SkyeyeBaseMapper<DocumentAuth> {
}
