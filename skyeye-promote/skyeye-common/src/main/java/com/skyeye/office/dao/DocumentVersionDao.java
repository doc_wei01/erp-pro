package com.skyeye.office.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.office.entity.DocumentVersion;

public interface DocumentVersionDao extends SkyeyeBaseMapper<DocumentVersion> {
}
