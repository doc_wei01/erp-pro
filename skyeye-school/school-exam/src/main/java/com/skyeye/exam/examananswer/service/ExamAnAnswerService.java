package com.skyeye.exam.examananswer.service;


import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.common.object.InputObject;
import com.skyeye.common.object.OutputObject;
import com.skyeye.exam.examananswer.entity.ExamAnAnswer;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: ExamAnAnswerService
 * @Description: 答卷/问答题保存表管理服务接口层
 * @author: skyeye云系列--lqy
 * @date: 2024/7/19 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface ExamAnAnswerService extends SkyeyeBusinessService<ExamAnAnswer> {
    void queryExamAnAnswerListById(InputObject inputObject, OutputObject outputObject);

    List<ExamAnAnswer> selectBySurveyId(String surveyId);
}
