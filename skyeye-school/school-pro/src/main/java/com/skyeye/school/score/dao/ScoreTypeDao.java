package com.skyeye.school.score.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.score.entity.ScoreType;

public interface ScoreTypeDao extends SkyeyeBaseMapper<ScoreType> {
}
