package com.skyeye.school.route.service;

import com.skyeye.base.business.service.SkyeyeBusinessService;
import com.skyeye.school.route.entity.RouteStop;

/**
 * @ClassName: RouteStopService
 * @Description: 路线站点服务管理接口
 * @author: skyeye云系列--lqy
 * @date: 2024/7/18 11:01
 * @Copyright: 2024 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface RouteStopService extends SkyeyeBusinessService<RouteStop> {
}
