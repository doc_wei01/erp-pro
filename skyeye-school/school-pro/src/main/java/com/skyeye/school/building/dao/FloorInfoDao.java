package com.skyeye.school.building.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.building.entity.FloorInfo;

/**
 * @ClassName: FloorInfoDao
 * @Description: 楼层教室服务数据层
 * @author: lqu
 * @date: 2023/9/5 17:16
 * @Copyright: 2023 https://gitee.com/doc_wei01/skyeye Inc. All rights reserved.
 * 注意：本内容仅限购买后使用.禁止私自外泄以及用于其他的商业目的
 */
public interface FloorInfoDao extends SkyeyeBaseMapper<FloorInfo> {
}
