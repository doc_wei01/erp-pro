package com.skyeye.school.chat.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.chat.entity.FriendRelationship;

public interface FriendRelationshipDao extends SkyeyeBaseMapper<FriendRelationship> {
}
