package com.skyeye.school.score.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.score.entity.ScoreSum;

public interface ScoreSumDao extends SkyeyeBaseMapper<ScoreSum> {
}
