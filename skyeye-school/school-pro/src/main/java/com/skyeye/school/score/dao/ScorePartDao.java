package com.skyeye.school.score.dao;

import com.skyeye.annotation.service.SkyeyeService;
import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.score.entity.ScorePart;

public interface ScorePartDao extends SkyeyeBaseMapper<ScorePart> {
}
