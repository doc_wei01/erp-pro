package com.skyeye.school.score.dao;

import com.skyeye.eve.dao.SkyeyeBaseMapper;
import com.skyeye.school.score.entity.ScoreTypeChild;

public interface ScoreTypeChildDao extends SkyeyeBaseMapper<ScoreTypeChild> {
}
